using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        private DBContext _context;

        public CustomerRepository()
        {
            _context = new DBContext();
        }

        public async void AddCustomer(Customer customer)
        {
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();
        }

        public async void AddListCustomer(List<Customer> customers)
        {
            foreach (var customer in customers)
            {
                _context.Customers.Add(customer);
            }
            await _context.SaveChangesAsync();
        }

        public Customer GetCustomer(int id)
        {
            return _context.Customers.Find(id);
        }
    }
}