﻿using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public List<Customer> Parse(string path)
        {
            var returner = new List<Customer>();
            using (var reader = new StreamReader(path))
            {
                var xmls = new XmlSerializer(typeof(List<Customer>), new XmlRootAttribute("Customers"));
                returner = (List<Customer>)xmls.Deserialize(reader);
            }
            return returner;
        }
    }
}