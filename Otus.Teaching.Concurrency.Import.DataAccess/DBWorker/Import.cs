﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DBContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseNpgsql("Host=localhost;Port=5432;Database=cust;Username=postgres;Password=postgres");
    }

    public class DBWorker
    {
        private readonly DBContext _context;

        public DBWorker()
        {
            _context = new DBContext();
        }

        public void Init()
        {
            _context.Database.EnsureCreated();
        }
    }

}
