using System;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class FakeDataLoader
        : IDataLoader
    {
        public void LoadData(string path)
        {

            var stopwatch = Stopwatch.StartNew();
            Thread.Sleep(14369);
            stopwatch.Stop();

            Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }
    }
}