﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;



namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = @"D:\repos\tst\customers.xml";
        
        static void Main(string[] args)
        {
            new DBWorker().Init();

            var serviceProvider = new ServiceCollection()
                .AddSingleton<IDataParser<List<Customer>>, XmlParser>()
                .AddSingleton<IDataLoader, DataLoader>()
                .BuildServiceProvider();

            if (args is {Length: 1})
            {
                _dataFilePath = args[0];
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            GenerateCustomersDataFile();

            var loader = serviceProvider.GetService<IDataLoader>();
            
            loader?.LoadData(_dataFilePath);
        }

        private static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(_dataFilePath, 1000);
            xmlGenerator.Generate();
        }

    }
}